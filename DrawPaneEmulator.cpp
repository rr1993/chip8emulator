/*
 * DrawPaneEmulator.cpp
 *
 *  Created on: Jan 18, 2016
 *      Author: robert
 */


#include "DrawPaneEmulator.hpp"
#include <iostream>
BEGIN_EVENT_TABLE(DrawPaneEmulator, wxPanel)
EVT_PAINT(DrawPaneEmulator::paintEvent)
END_EVENT_TABLE()


DrawPaneEmulator::DrawPaneEmulator(wxFrame* parent) : wxPanel(parent){
	//Set our state to 0
	for(unsigned short i = 0; i < 2048; ++i){
		panelState.at(i) = 0;
	}

	//Set current color to black
	currentColor = 0;

}

void DrawPaneEmulator::paintEvent(wxPaintEvent & evt)
{
	drawFullScreen();
}

void DrawPaneEmulator::clear(){
	wxClientDC dc(this);
    dc.SetBrush(wxColor(0, 0, 0));
    dc.SetPen(wxPen(wxColor(0, 0, 0)));
	dc.DrawRectangle(0, 0, 1280, 640);
	currentColor = 0;
}

void DrawPaneEmulator::setGPU(const std::array<uint8_t, (64 * 32)>* gpu){
	wxClientDC dc(this);

	bool newPixelDrawn = false; //Anti-flickering mechanism
	for(unsigned short i = 0; i < 32; ++i){
		for(unsigned short j = 0; j < 64; ++j){
			unsigned short pixelStatus = gpu->at((i * 64) + j);
			if(setPixel(j, i, pixelStatus, dc) == true){
				newPixelDrawn = true;
			}
		}
	}

	if(newPixelDrawn)
		drawFullScreen();
}

void DrawPaneEmulator::changePixel(uint8_t x, uint8_t y, uint8_t state){
	panelState.at((y * 64) + x) = state;
}


bool DrawPaneEmulator::setPixel(uint8_t xPos, uint8_t yPos, uint8_t state, wxDC& dc){
	unsigned short pixel = (yPos * 64) + xPos;

	//If the state of our screen is different set the pixel to an other color.
	
	if(panelState.at(pixel) != state){
		panelState.at(pixel) = state;

		if(state == 1){
			return true;
		}
	}

	return false;
}

void DrawPaneEmulator::drawFullScreen(){
	wxClientDC adc(this);
	wxBufferedDC dc(&adc);

	unsigned short xPos = 0;
	unsigned short yPos = 0;
	for(uint8_t i = 0; i < 32; ++i){
		for(uint8_t j = 0; j < 64; ++j){
			unsigned short pixelStatus = panelState.at((i * 64) + j);
			setRightColor(pixelStatus, dc);
			dc.DrawRectangle(xPos, yPos, 20, 20);
			xPos += 20;
		}
		yPos += 20;
		xPos = 0;
	}
}

void DrawPaneEmulator::setRightColor(uint8_t state, wxDC& dc){

	if(state == 1){
	    dc.SetBrush(wxColor(255, 255, 255));
	    dc.SetPen( wxPen( wxColor(255, 255, 255), 1 ));
	    currentColor = 1;
	} else if(state == 0) {
	    dc.SetBrush(wxColor(0, 0, 0));
	    dc.SetPen( wxPen( wxColor(0, 0, 0), 0 ));
		currentColor = 0;
	} else {
		std::cout << "Strange state : " << (int)state << std::endl;
	}
}


