/*
 * DrawPaneEmulator.hpp
 *
 *  Created on: Jan 18, 2016
 *      Author: robert
 */

#ifndef DRAWPANEEMULATOR_HPP_
#define DRAWPANEEMULATOR_HPP_

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
#include <stdint.h>
#include <wx/dcbuffer.h>
#include <array>

class DrawPaneEmulator : public wxPanel{
public:
	/**
	 * @brief Constructor of the drawPane
	 */
	DrawPaneEmulator(wxFrame* parent);

	/**
	 * @brief Used to catch drawevents, redraws the full screen
	 */
	void paintEvent(wxPaintEvent & evt);

	/**
	 * @brief set the interal state of the panel to the given memory array
	 */
	void setGPU(const std::array<uint8_t, (64 * 32)>* gpu);


	/**
	 * @brief change a single pixel on the screen
	 */
	void changePixel(uint8_t x, uint8_t y, uint8_t state);


	/**
	 * @brief Clears the screen
	 */
	void clear();


	DECLARE_EVENT_TABLE()


private:
	std::array<uint8_t, (32 * 64)>panelState;
	uint8_t currentColor;

	/**
	 * @brief Change the state of one pixel in the memory of the drawPanel.
	 * returns true if a pixel is set to lit, and false if pixels only have been turned off
	 */
	bool setPixel(uint8_t xPos, uint8_t yPos, uint8_t state, wxDC& dc);

	/**
	 * @brief Draw the full memory array to the actual screen
	 */
	void drawFullScreen();

	/**
	 * @brief Sets the right brush color when needed
	 */
	void setRightColor(uint8_t state, wxDC& dc);

};



#endif /* DRAWPANEEMULATOR_HPP_ */
