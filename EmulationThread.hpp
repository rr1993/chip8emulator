/*
 * EmulationThread.hpp
 *
 *  Created on: Jan 19, 2016
 *      Author: robert
 */

#ifndef EMULATIONTHREAD_HPP_
#define EMULATIONTHREAD_HPP_

#include "EmulatorApp.hpp"
#include "DrawPaneEmulator.hpp"
#include "chip8.hpp"
#include "wx/wx.h"
#include <fstream>
#include <boost/bind.hpp>
#include <string>

class EmulationThread : public wxThread{
public:

	/**
	 * Constructor
	 */
	EmulationThread(wxFrame *parent, DrawPaneEmulator* aDrawPane, std::string aRomPath) : emulationFrame(parent), drawPanel(aDrawPane), keepRunning(true), romPath(aRomPath){};


	/**
	 * The main function of the new thread
	 */
	virtual ExitCode Entry(){
		readROM(romPath, chip8);

		while(keepRunning){
			chip8.emulateStep();
			if(chip8.getNeedsRedraw()){
				chip8.resetNeedsRedraw();
				emulationFrame->GetEventHandler()->CallAfter(boost::bind(&DrawPaneEmulator::setGPU,drawPanel,chip8.getGPU()));
			}
			
			//Slow down the emulation
			usleep(500);
		}
	};

	/**
	 * @brief Read a romfile and load it into the memory of the chip8 emulator
	 */
	void readROM(std::string aRomName, Chip8 &theChip){
		std::fstream romFile;
		uint8_t* code = new uint8_t[1024];
		romFile.open(aRomName.c_str(), std::ios::in | std::ios::binary);
		if(!romFile.is_open()){
			std::cout << "Couldnt read romfile : " << aRomName << std::endl;
			return;
		}

		//Get the size of the file
		std::streampos begin, end;
		begin = romFile.tellg();
		romFile.seekg(0, std::ios::end);
		end = romFile.tellg();
		romFile.seekg(0, std::ios::beg);
		uint16_t length = end - begin;

		//Read to code buffer
		char c;
		for(uint16_t i = 0; i < length; i++){
			romFile.get(c);
			code[i] = (uint8_t)c;
		}

		theChip.loadCode(code, length);
	}

	/**
	 * @brief Register a KeyPress
	 */
	void registerKeyPress(uint8_t aKey){
		chip8.registerKeyPress(aKey);
	}

	/**
	 * @brief Register a KeyRelease
	 */
	void registerKeyRelease(uint8_t aKey){
		chip8.registerKeyRelease(aKey);
	}
	
	/**
	 * @brief Change the keepRunning flag so the thread will return / terminate
	 */
	void stopThread(){
		keepRunning = false;
	}


private:
	wxFrame *emulationFrame;
	DrawPaneEmulator *drawPanel;
	Chip8 chip8;
	std::string romPath;
	
	bool keepRunning;
};




#endif /* EMULATIONTHREAD_HPP_ */
