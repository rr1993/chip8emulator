/*
 * EmulatorApp.cpp
 *
 *  Created on: Jan 18, 2016
 *      Author: robert
 */

#include "EmulatorApp.hpp"
#include "EmulatorFrame.hpp"

wxIMPLEMENT_APP(EmulatorApp);

bool EmulatorApp::OnInit(){
	EmulatorFrame* frame = new EmulatorFrame;
	frame->Show(true);
	return true;
}

