/*
 * EmulatorApp.hpp
 *
 *  Created on: Jan 18, 2016
 *      Author: robert
 */

#ifndef EMULATORAPP_HPP_
#define EMULATORAPP_HPP_

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

class EmulatorApp : public wxApp{
public:
	virtual bool OnInit();
};



#endif /* EMULATORAPP_HPP_ */
