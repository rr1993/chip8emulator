/*
 * EmulatorFrame.cpp
 *
 *  Created on: Jan 18, 2016
 *      Author: robert
 */
#include "EmulatorFrame.hpp"
#include <iostream>
#include <string>
#include <iostream>

enum{
	ID_Load_rom = 10,
	ID_Stop_rom = 11,
	
	ID_about = 20
};


wxBEGIN_EVENT_TABLE(EmulatorFrame, wxFrame)
	EVT_MENU(ID_Load_rom, EmulatorFrame::onStartClicked)
	EVT_MENU(ID_Stop_rom, EmulatorFrame::onStopClicked)
	
	EVT_MENU(ID_about, EmulatorFrame::onAboutClicked)
	EVT_CHAR_HOOK(EmulatorFrame::handleKeyPress)
wxEND_EVENT_TABLE()



//Scaling to factor 20, pixels will  be 20, 20
EmulatorFrame::EmulatorFrame() : wxFrame(NULL, wxID_ANY, "CHIP8 Emulator - rr1993", wxDefaultPosition, wxSize(1280, 660), wxWANTS_CHARS),
		aDrawPane(nullptr), aThread(nullptr){
	//Create the menu
	wxMenuBar *menuBar = new wxMenuBar;

	//Create menu items for file menu
	wxMenu *menuFile = new wxMenu;
	menuFile->Append(ID_Load_rom, "Load ROM");
	menuFile->Append(ID_Stop_rom, "Stop ROM");
	menuFile->AppendSeparator();

	//Create menu items for about menu
	wxMenu *menuAbout = new wxMenu;
	menuAbout->Append(20, "About");


	menuBar->Append(menuFile, "File");
	menuBar->Append(menuAbout, "About");
	SetMenuBar(menuBar);

	//Draw pane
	aDrawPane = new DrawPaneEmulator(this);

	//Create a sizer
	wxBoxSizer* box_sizer_emulator = new wxBoxSizer(wxHORIZONTAL);
	SetSizer(box_sizer_emulator);

	box_sizer_emulator->Add(aDrawPane, 1, wxEXPAND);

	aDrawPane->clear();

}

void EmulatorFrame::onStartClicked(wxCommandEvent& event){
	//Open a file dialog and get the path to the selected ROM file.
	wxFileDialog *openFileDialog = new wxFileDialog(this);
	std::string fileName = "";
	if(openFileDialog->ShowModal() == wxID_OK){
		fileName = openFileDialog->GetPath();
		std::cout << "Trying to open : " << fileName << std::endl;
	}
	
	
	aThread = new EmulationThread(this, aDrawPane, fileName);
	aThread->Create();
	aThread->Run();
}

void EmulatorFrame::onStopClicked(wxCommandEvent& event)
{
	//Let the thread return
	aThread->stopThread();
}



void EmulatorFrame::handleKeyPress(wxKeyEvent& aKeyEvent){
	wxChar uc = aKeyEvent.GetUnicodeKey();

	//Check if the emulation thread exists and is running before sending it events
	if(aThread == nullptr || !aThread->IsRunning()){
		return;
	}
	
	
	switch(uc){
	case '1':
		aThread->registerKeyPress(1);
		break;
	case '2':
		aThread->registerKeyPress(2);
		break;
	case '3':
		aThread->registerKeyPress(3);
		break;
	case '4':
		aThread->registerKeyPress(4);
		break;
	case '5':
		aThread->registerKeyPress(5);
		break;
	case '6':
		aThread->registerKeyPress(6);
		break;
	case '7':
		aThread->registerKeyPress(7);
		break;
	case '8':
		aThread->registerKeyPress(8);
		break;
	case '9':
		aThread->registerKeyPress(9);
		break;
	case 'A':
		aThread->registerKeyPress(10);
		break;
	case 'B':
		aThread->registerKeyPress(11);
		break;
	case 'C':
		aThread->registerKeyPress(12);
		break;
	case 'D':
		aThread->registerKeyPress(13);
		break;
	case 'E':
		aThread->registerKeyPress(14);
		break;
	case 'F':
		aThread->registerKeyPress(15);
		break;
	}
}


void EmulatorFrame::onAboutClicked(wxCommandEvent& event){
	wxMessageBox("CHIP8 Emulator, made by rr1993", "CHIP8 Emulator", wxOK | wxICON_INFORMATION);
}
