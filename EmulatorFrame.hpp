/*
 * EmulatorFrame.hpp
 *
 *  Created on: Jan 16, 2016
 *      Author: robert
 */

#ifndef EMULATORFRAME_HPP_
#define EMULATORFRAME_HPP_

#include "EmulationThread.hpp"
#include "DrawPaneEmulator.hpp"
#include <thread>

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif


class EmulatorFrame : public wxFrame{

public:
	EmulatorFrame();

private:
	wxDECLARE_EVENT_TABLE();
	DrawPaneEmulator* aDrawPane;
	EmulationThread * aThread;


	/**
	 * @brief Handle the click event on the menu item About.
	 */
	void onAboutClicked(wxCommandEvent& event);

	/**
	 * @brief Handle the click event on the menu item Load ROM.
	 */
	void onStartClicked(wxCommandEvent& event);
	
	/**
	 * @brief Handle the click event on the menu item Stop ROM.
	 */
	void onStopClicked(wxCommandEvent& event);

	/**
	 * @brief Handle key presses and send them to the emulator as input
	 */
	void handleKeyPress(wxKeyEvent& aKeyEvent);




};



#endif /* EMULATORFRAME_HPP_ */
