/*
 * chip8.cpp
 *
 *  Created on: Jan 15, 2016
 *      Author: robert
 */


#include "chip8.hpp"
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <ctime>

static uint8_t fontset_data[80] =
{
  0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
  0x20, 0x60, 0x20, 0x20, 0x70, // 1
  0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
  0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
  0x90, 0x90, 0xF0, 0x10, 0x10, // 4
  0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
  0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
  0xF0, 0x10, 0x20, 0x40, 0x40, // 7
  0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
  0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
  0xF0, 0x90, 0xF0, 0x90, 0x90, // A
  0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
  0xF0, 0x80, 0x80, 0x80, 0xF0, // C
  0xE0, 0x90, 0x90, 0x90, 0xE0, // D
  0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
  0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

const uint8_t* Chip8::fontset = fontset_data;



Chip8::Chip8() : memoryPointer(0), IP(0x200), delayTimer(0), soundTimer(0), needsRedraw(false), waitingForKeyPress(false){
	reset();
}

void Chip8::emulateStep(){
	//Fetch one opcode.
	uint16_t opcode = memory.at(IP) << 8 | memory.at(IP + 1);
	parseOpCode(opcode);
}

void Chip8::setInputState(uint8_t anInputRegister, uint8_t aState){
	if(anInputRegister >= 16 || aState > 1){
		//Invalid value's
		return;
	}

	input[anInputRegister] = aState;
}

void Chip8::reset(){
	//Set memory to 0
	for(uint16_t i = 0; i < 4096; ++i){
		memory.at(i) = 0;
	}
	//Set memory pointer to 0
	memoryPointer = 0;

	//Reset the Instruction Pointer to the start of the code section
	IP = 0x200;

	//Set registers to zero
	for(uint8_t i = 0; i < 16; ++i){
		reg.at(i) = 0;
	}

	//Reset the screen
	for(uint16_t i = 0; i < (64 * 32); ++i){
		gfx.at(i) = 0;
	}

	//Reset the input registers
	for(uint8_t i = 0; i < 16; ++i){
		input[i] = 0;
	}

	//Load fonts to memory
	for(uint8_t i = 0; i < 80; ++i){
		memory.at(i) = fontset[i];
	}

	//Seed the random number generator
	std::srand (std::time(NULL));
}

void Chip8::loadCode(uint8_t *code, uint16_t len){
	for(uint16_t i = 0; i < len; ++i){
		memory.at(IP + i) = code[i];
	}
}

void Chip8::registerKeyPress(uint8_t aKey){
	if(aKey > 16 || aKey < 1){
		//Only keys in range from 1 to 16 (inclusive) are valid
		return;
	}
	aKey = aKey - 1; //From key to index


	if(waitingForKeyPress){
		reg[registerToStoreKeyPress] = aKey;
		keyWaitCondition.notify_one(); //Notify the condition variable about the keypress
		waitingForKeyPress = false;
	}

	input[aKey] = 1; //Set the state of the input register
}

void Chip8::registerKeyRelease(uint8_t aKey){
	if(aKey > 16 || aKey < 1){
		//Only keys in range from 1 to 16 (inclusive) are valid
		return;
	}
	aKey = aKey - 1; //From key to index

	input[aKey] = 0;
}

const std::array<uint8_t, (64 * 32)>* Chip8::getGPU(){
	return &gfx;
}

bool Chip8::getNeedsRedraw()const{
	return needsRedraw;
}

void Chip8::resetNeedsRedraw(){
	needsRedraw = false;
}



/* Private */
void Chip8::parseOpCode(uint16_t aOpcode){
	std::cout << std::hex << aOpcode << std::dec << std::endl;

	if(aOpcode == 0x00E0){
		clearScreen();
	}else if(aOpcode == 0x00EE){
		returnFromSubroutine();
	}else if(((aOpcode & 0xF000) >> 12) == 1){
		jumpToAddress(aOpcode);
	}else if (((aOpcode & 0xF000) >> 12) == 2){
		callSubroutine(aOpcode);
	}else if (((aOpcode & 0xF000) >> 12) == 3){
		skipNextInstructionOnMatch(aOpcode);
	}else if (((aOpcode & 0xF000) >> 12) == 4){
		skipNextInstructionNoMatch(aOpcode);
	}else if (((aOpcode & 0xF000) >> 12) == 5){
		skipNextInstructionMatchReg(aOpcode);
	}else if (((aOpcode & 0xF000) >> 12) == 6){
		setRegToValue(aOpcode);
	}else if (((aOpcode & 0xF000) >> 12) == 7){
		addValueToReg(aOpcode);
	}else if((aOpcode & 0xF00F) == 0x8000){
		setRegToRegValue(aOpcode);
	}else if((aOpcode & 0xF00F) == 0x8001){
		setRegToRegValueOR(aOpcode);
	}else if((aOpcode & 0xF00F) == 0x8002){
		setRegToRegValueAND(aOpcode);
	}else if((aOpcode & 0xF00F) == 0x8003){
		setRegToRegValueXOR(aOpcode);
	}else if((aOpcode & 0xF00F) == 0x8004){
		addRegToRegCarry(aOpcode);
	}else if((aOpcode & 0xF00F) == 0x8005){
		subRegFromRegCarry(aOpcode);
	}else if((aOpcode & 0xF00F) == 0x8006){
		shiftRegRightCarry(aOpcode);
	}else if((aOpcode & 0xF00F) == 0x8007){
		setSubRegToRegCarry(aOpcode);
	}else if((aOpcode & 0xF00F) == 0x800E){
		shiftRegLeftCarry(aOpcode);
	}else if((aOpcode & 0xF00F) == 0x9000){
		skipNextInstructionNoMatchReg(aOpcode);
	}else if(((aOpcode & 0xF000) >> 12) == 0xA){
		setMemoryPointer(aOpcode);
	}else if(((aOpcode & 0xF000) >> 12) == 0xB){
		jumpToAddressAddReg(aOpcode);
	}else if(((aOpcode & 0xF000) >> 12) == 0xC){
		setRegToRandValueAND(aOpcode);
	}else if(((aOpcode & 0xF000) >> 12) == 0xD){
		displaySpriteFromMem(aOpcode);
	}else if((aOpcode & 0xF0FF) == 0xE09E){
		skipIfPressed(aOpcode);
	}else if((aOpcode & 0xF0FF) == 0xE0A1){
		skipIfNotPressed(aOpcode);
	}else if((aOpcode & 0xF0FF) == 0xF007){
		timerValueInReg(aOpcode);
	}else if((aOpcode & 0xF0FF) == 0xF00A){
		waitForKeyPress(aOpcode);
	}else if((aOpcode & 0xF0FF) == 0xF015){
		setDelayTimerToRegValue(aOpcode);
	}else if((aOpcode & 0xF0FF) == 0xF018){
		setSoundTimerToRegValue(aOpcode);
	}else if((aOpcode & 0xF0FF) == 0xF01E){
		addRegValueToMemPointer(aOpcode);
	}else if((aOpcode & 0xF0FF) == 0xF029){
		setMemPointerToSprite(aOpcode);
	}else if((aOpcode & 0xF0FF) == 0xF033){
		storeBDCInRegs(aOpcode);
	}else if((aOpcode & 0xF0FF) == 0xF055){
		storeRegsInMemory(aOpcode);
	}else if((aOpcode & 0xF0FF) == 0xF065){
		putMemoryValuesInRegs(aOpcode);
	}else {
		std::cout << "Couldnt handle opcode !! : " << aOpcode << std::endl;
	}

	if(delayTimer > 0)
		delayTimer--;

	if(soundTimer > 0)
		soundTimer--;
}

/* Private */
void Chip8::printScreenToCout(){
	for(uint8_t i = 0; i < 32; ++i){
		for(uint8_t j =0; j < 64; ++j){
			uint16_t currentPos = ((64 * i) + j);
			if(gfx.at(currentPos) == 1)
				std::cout << "*";
			else
				std::cout << " ";
		}
		std::cout << "\n";
	}

	std::cout << std::endl;
}


/* Private */
void Chip8::clearScreen(){
	for(uint16_t i = 0; i < (64 * 32); ++i){
		gfx.at(i) = 0;
	}

	IP += 2;
}

/* Private */
void Chip8::returnFromSubroutine(){
	uint16_t returnAddress = stack.top();
	stack.pop();

	IP = returnAddress + 2;

	std::cout << "Returned from routine !" << std::endl;
}

/* Private */
void Chip8::jumpToAddress(uint16_t aOpcode){
	uint16_t address = aOpcode & 0x0FFF;
	IP = address;
}

/* Private */
void Chip8::callSubroutine(uint16_t aOpcode){
	uint16_t currentAddress = IP;
	uint16_t callAddress = (aOpcode & 0x0FFF);
	stack.push(currentAddress);

	std::cout << "Called subroutine" << std::endl;

	if(stack.size() > 15){
		std::cout << "Stack overflow !" << std::endl;
	}

	IP = callAddress;
}

/* Private */
void Chip8::skipNextInstructionOnMatch(uint16_t aOpcode){
	uint8_t aReg = (aOpcode & 0x0F00) >> 8;
	uint8_t aValue = aOpcode & 0x00FF;

	if(reg.at(aReg) == aValue){
		IP += 4;
	} else {
		IP += 2;
	}
}

/* Private */
void Chip8::skipNextInstructionNoMatch(uint16_t aOpcode){
	uint8_t aReg = (aOpcode & 0x0F00) >> 8;
	uint8_t aValue = aOpcode & 0x00FF;

	std::cout << "Skip next no match" << std::endl;

	if(reg.at(aReg) != aValue){
		IP += 4;
	} else {
		IP += 2;
	}
}

/* Private */
void Chip8::skipNextInstructionMatchReg(uint16_t aOpcode){
	uint8_t aReg1 = (aOpcode & 0x0F00) >> 8;
	uint8_t aReg2 = (aOpcode & 0x00F0) >> 4;

	if(reg.at(aReg1) == reg.at(aReg2)){
		IP += 4;
	} else {
		IP += 2;
	}
}

/* Private */
void Chip8::setRegToValue(uint16_t aOpcode){
	uint8_t aReg = (aOpcode & 0x0F00) >> 8;
	uint8_t aValue = aOpcode & 0x00FF;

	reg.at(aReg) = aValue;
	IP += 2;
}

/* Private */
void Chip8::addValueToReg(uint16_t aOpcode){
	uint8_t aReg = (aOpcode & 0x0F00) >> 8;
	uint8_t aValue = aOpcode & 0x00FF;

	reg.at(aReg) += aValue;
	IP += 2;
}

/* Private */
void Chip8::setRegToRegValue(uint16_t aOpcode){
	uint8_t aReg1 = (aOpcode & 0x0F00) >> 8;
	uint8_t aReg2 = (aOpcode & 0x00F0) >> 4;

	reg.at(aReg1) = reg.at(aReg2);
	IP += 2;
}

void Chip8::setRegToRegValueOR(uint16_t aOpcode) {
	uint8_t aReg1 = (aOpcode & 0x0F00) >> 8;
	uint8_t aReg2 = (aOpcode & 0x00F0) >> 4;

	reg.at(aReg1) = (reg.at(aReg1) | reg.at(aReg2));
	IP += 2;
}

void Chip8::setRegToRegValueAND(uint16_t aOpcode) {
	uint8_t aReg1 = (aOpcode & 0x0F00) >> 8;
	uint8_t aReg2 = (aOpcode & 0x00F0) >> 4;

	reg.at(aReg1) = (reg.at(aReg1) & reg.at(aReg2));
	IP += 2;
}

void Chip8::setRegToRegValueXOR(uint16_t aOpcode) {
	uint8_t aReg1 = (aOpcode & 0x0F00) >> 8;
	uint8_t aReg2 = (aOpcode & 0x00F0) >> 4;

	reg.at(aReg1) = (reg.at(aReg1) ^reg.at(aReg2));
	IP += 2;
}

void Chip8::addRegToRegCarry(uint16_t aOpcode) {
	uint8_t aReg1 = (aOpcode & 0x0F00) >> 8;
	uint8_t aReg2 = (aOpcode & 0x00F0) >> 4;

	if((static_cast<uint16_t>(reg.at(aReg1)) + static_cast<uint16_t>(reg.at(aReg2))) > 255){
		std::cout << "Setted carry !" << std::endl;
		reg.at(15) = 1; //Set carryflag
	} else {
		std::cout << "Didnt set carry !" << std::endl;
		reg.at(15) = 0;
	}
	
	reg.at(aReg1) += reg.at(aReg2);

	IP += 2;
}

void Chip8::subRegFromRegCarry(uint16_t aOpcode) {
	uint8_t aReg1 = (aOpcode & 0x0F00) >> 8;
	uint8_t aReg2 = (aOpcode & 0x00F0) >> 4;

	if((static_cast<int16_t>(reg.at(aReg1)) - static_cast<int16_t>(reg.at(aReg2))) >= 0){
		reg.at(15) = 0;
	} else {
		reg.at(15) = 1;
	}

	reg.at(aReg1) -= reg.at(aReg2);

	IP += 2;
}

void Chip8::shiftRegRightCarry(uint16_t aOpcode) {
	uint8_t aReg1 = (aOpcode & 0x0F00) >> 8;
	uint8_t aReg2 = (aOpcode & 0x00F0) >> 4;

	reg.at(15) = (reg.at(aReg2) & 7);
	reg.at(aReg1) = (reg.at(aReg2) >> 1);

	IP += 2;

}

void Chip8::setSubRegToRegCarry(uint16_t aOpcode) {
	uint8_t aReg1 = (aOpcode & 0x0F00) >> 8;
	uint8_t aReg2 = (aOpcode & 0x00F0) >> 4;

	if((static_cast<int16_t>(reg.at(aReg1)) - static_cast<int16_t>(reg.at(aReg2))) > 0){
		reg.at(15) = 1;
	} else {
		reg.at(15) = 0;
	}

	reg.at(aReg1) = reg.at(aReg2) - reg.at(aReg1);
	IP += 2;
}

void Chip8::shiftRegLeftCarry(uint16_t aOpcode) {
	uint8_t aReg1 = (aOpcode & 0x0F00) >> 8;

	reg.at(15) = reg.at(aReg1) >> 7;
	reg.at(aReg1) = reg.at(aReg1) << 1;

	IP += 2;
}

/* Private */
void Chip8::skipNextInstructionNoMatchReg(uint16_t aOpcode){
	uint8_t aReg1 = (aOpcode & 0x0F00) >> 8;
	uint8_t aReg2 = (aOpcode & 0x00F0) >> 4;

	if(reg.at(aReg1) != reg.at(aReg2)){
		IP += 4;
	} else {
		IP += 2;
	}
}

/* Private */
void Chip8::setMemoryPointer(uint16_t aOpcode){
	uint16_t address = aOpcode & 0x0FFF;
	memoryPointer = address;

	IP += 2;
}


/* Private */
void Chip8::jumpToAddressAddReg(uint16_t aOpcode){
	uint16_t address = aOpcode & 0x0FFF;
	address += reg.at(0);
	IP = address;
}

/* Private */
void Chip8::setRegToRandValueAND(uint16_t aOpcode){
	uint8_t aReg1 = (aOpcode & 0x0F00) >> 8;
	reg.at(aReg1) = std::rand() & (aOpcode & 0x00FF);
	
	IP += 2;
}


/* Private */
void Chip8::displaySpriteFromMem(uint16_t aOpcode){
  
	unsigned short x = reg.at((aOpcode & 0x0F00) >> 8);
        unsigned short y = reg.at((aOpcode & 0x00F0) >> 4);
        unsigned short height = aOpcode & 0x000F;
        unsigned short pixel;

        reg.at(0xF) = 0;
        for (int yline = 0; yline < height; yline++)
        {
	  pixel = memory.at(memoryPointer + yline);
          for(int xline = 0; xline < 8; xline++)
	    {
	      if((pixel & (0x80 >> xline)) != 0)
	      {
		  if(gfx[(x + xline + ((y + yline) * 64))] == 1)
		  {
		    reg.at(0xF) = 1;
                  }
                    gfx[x + xline + ((y + yline) * 64)] ^= 1;
		  }
              }
         }

            needsRedraw = true;
            IP += 2;
  
  
	/*
	uint8_t aReg1 = (0x0F00 & aOpcode) >> 8; //Register storing the X position to draw the sprite to
	uint8_t aReg2 = (0x00F0 & aOpcode) >> 4; //Register storing the Y position to draw the sprite to
	uint8_t sprite_height = (0x000F & aOpcode);

	uint8_t xPos = reg.at(aReg1);
	uint8_t yPos = reg.at(aReg2);
	//uint16_t pixel = 0;


	//Set the collision flag to 0 by default;
	reg.at(15) = 1;


	for(uint8_t row = 0; row < sprite_height; ++row){
		uint16_t absoluteYPos = (yPos + row);
		uint8_t pixelData = memory.at(memoryPointer + row);

		reg.at(15) = 0;
		
		//Screen wrapping
		while(absoluteYPos >= 32){
			absoluteYPos -= 32;
		}

		for(uint8_t col = 0; col < 8; ++col){
			uint16_t absoluteXPos = (xPos + col);
			//Screen wrapping
			while(absoluteXPos >= 64){
				absoluteXPos -= 64;
			}
			
			if((pixelData & (0x80 >> col)) != 0){
				if(gfx.at((absoluteYPos * 64) + absoluteXPos) == 1){
					reg.at(15) = 1;
				}
				gfx.at((absoluteYPos * 64) + absoluteXPos) ^= 1;
			} else {
			    if(gfx.at((absoluteYPos * 64) + absoluteXPos) == 0){
			      reg.at(15) = 1;
			    }
			}
		}
	}

	needsRedraw = true;

	//printScreenToCout();

	IP += 2; */

}

/* Private */
void Chip8::skipIfPressed(uint16_t aOpcode){
	uint8_t inputRegister = (0x0F00 & aOpcode) >> 8;
	if(input[inputRegister] == 1){
		std::cout << "Skip if pressed : skipped !" << std::endl;
		IP += 4;
	} else {
		std::cout << "Skip if pressend : not skipped !" << std::endl;
		IP += 2;
	}

	input[inputRegister] = 0;
}

/* Private */
void Chip8::skipIfNotPressed(uint16_t aOpcode){
	uint8_t inputRegister = (0x0F00 & aOpcode) >> 8;
	if(input[inputRegister] == 1){
		IP += 2;
	} else {
		IP += 4;
	}
	
	input[inputRegister] = 0;
}

/* Private */
void Chip8::timerValueInReg(uint16_t aOpcode){
	uint8_t aReg1 = (0x0F00 & aOpcode) >> 8;
	reg.at(aReg1) = delayTimer;

	IP += 2;
}

/* Private */
void Chip8::waitForKeyPress(uint16_t aOpcode){
	std::unique_lock<std::mutex> lock(keyWait);
	uint8_t aReg1 = (0x0F00 & aOpcode) >> 8;

	waitingForKeyPress = true; //Set the flag to true;
	registerToStoreKeyPress = aReg1;

	keyWaitCondition.wait(lock); //Wait for a keypress


	IP += 2;
}

/* Private */
void Chip8::setDelayTimerToRegValue(uint16_t aOpcode){
	uint8_t aReg1 = (0x0F00 & aOpcode) >> 8;
	delayTimer = reg.at(aReg1);

	std::cout << "Delaytimer : " << (int)delayTimer << std::endl;

	IP += 2;
}

/* Private */
void Chip8::setSoundTimerToRegValue(uint16_t aOpcode){
	uint8_t aReg1 = (0x0F00 & aOpcode) >> 8;
	soundTimer = reg.at(aReg1);

	IP += 2;
}

/* Private */
void Chip8::addRegValueToMemPointer(uint16_t aOpcode){
	uint8_t aReg1 = (0x0F00 & aOpcode) >> 8;
	memoryPointer += reg.at(aReg1);

	IP += 2;
}

/* Private */
void Chip8::setMemPointerToSprite(uint16_t aOpcode){
	uint8_t aReg1 = (0x0F00 & aOpcode) >> 8;

	memoryPointer = reg.at(aReg1) * 0x5;
	IP += 2;
}

/* Private */
void Chip8::storeBDCInRegs(uint16_t aOpcode){
	uint8_t aReg1 = (0x0F00 & aOpcode) >> 8;

	memory.at(memoryPointer) = reg.at(aReg1) / 100;
	memory.at(memoryPointer + 1) = (reg.at(aReg1) / 10) % 10;
	memory.at(memoryPointer + 2) = (reg.at(aReg1) / 100) % 10;

	IP += 2;
}

/* Private */
void Chip8::storeRegsInMemory(uint16_t aOpcode){
	uint8_t saveUntil = (0x0F00 & aOpcode) >> 8;
	saveUntil++;

	for(uint8_t i = 0; i < saveUntil; ++i){
		memory.at(memoryPointer + i) = reg.at(i);
		std::cout << "Stored : " << (int)reg.at(i) << " at " << (memoryPointer + i) << std::endl;
	}

	memoryPointer += saveUntil + 1;

	IP += 2;
}

/* Private */
void Chip8::putMemoryValuesInRegs(uint16_t aOpcode){
	uint8_t getUntil = (0x0F00 & aOpcode) >> 8;
	getUntil++;

	for(uint8_t i = 0; i < getUntil; ++i){
		reg.at(i) = memory.at(memoryPointer + i);
		std::cout << "Got : " << (int)reg.at(i) << " in reg " << (int)i << " from " << (memoryPointer + i) << std::endl;
	}

	memoryPointer += getUntil + 1;

	IP += 2;
}



