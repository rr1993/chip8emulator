/*
 * chip8.hpp
 *
 *  Created on: Jan 15, 2016
 *      Author: robert
 */

#ifndef CHIP8_HPP_
#define CHIP8_HPP_

#include <stdint.h>
#include <stack>
#include <iostream>
#include <array>
#include <mutex>
#include <condition_variable>


class Chip8{
public:

	Chip8();


	/**
	 * @brief Run one emulation step, or execute one single opcode.
	 */
	void emulateStep();

	/**
	 * @brief Set the state of an certain input
	 *
	 * @param aInputRegister The input register to change its state
	 *
	 * @param aState The current state of the input, 0 = not pressed, 1 = pressed
	 */
	void setInputState(uint8_t anInputRegister, uint8_t aState);

	/**
	 * @brief Reset the chip8 emulator to a startup state
	 */
	void reset();

	/**
	 * @brief Get the GPU memory
	 */
	const std::array<uint8_t, (64 * 32)>* getGPU();

	/**
	 * @brief Load the code of a rom to the memory
	 *
	 * @param aCode array with the code to be played by the emulator
	 *
	 * @param len Length / size of the code array
	 */
	void loadCode(uint8_t* code, uint16_t len);

	/**
	 * @brief register KeyPress
	 */
	void registerKeyPress(uint8_t aKey);

	/**
	 * @brief register keyRelease
	 */
	void registerKeyRelease(uint8_t aKey);

	/**
	 * Getter to check if the panel needs a redraw
	 */
	bool getNeedsRedraw()const;

	/**
	 * Reset the needsRedraw flag when the actual redraw is done.
	 */
	void resetNeedsRedraw();





private:
	//Memory of the chip
	std::array<uint8_t, 4096>memory;
	uint16_t memoryPointer;

	//Instruction pointer
	uint16_t IP;

	//Locking mechanisms to stop the executing thread if a keypress is awaited
	std::mutex keyWait;
	std::condition_variable keyWaitCondition;

	//Timers
	uint8_t delayTimer;
	uint8_t soundTimer;

	//Stack
	std::stack<uint16_t> stack;

	//registers of the chip
	std::array<uint8_t, 16>reg;

	//graphic memory (64 * 32 pixels)
	std::array<uint8_t, (64 * 32)>gfx;

	//Input registers
	uint8_t input[16];

	//Flag to state if the screen needs redrawing
	bool needsRedraw;

	//Keypress waiting
	bool waitingForKeyPress;
	uint8_t registerToStoreKeyPress;

	/* Helper functions */
	
	/**
	 * @brief Parses the given opcode and execute's it
	 * 
	 * @param aOpcode The opcode to be parsed.
	 */
	void parseOpCode(uint16_t aOpcode);

	/**
	 * @brief prints the current screen to cout
	 */
	void printScreenToCout();



	/* Opcode functions */
	void clearScreen(); //00E0
	void returnFromSubroutine(); //00EE
	void jumpToAddress(uint16_t aOpcode); //1NNN
	void callSubroutine(uint16_t aOpcode); //2NNN
	void skipNextInstructionOnMatch(uint16_t aOpcode); //3XNN
	void skipNextInstructionNoMatch(uint16_t aOpcode); //4XNN
	void skipNextInstructionMatchReg(uint16_t aOpcode); //5XY0
	void setRegToValue(uint16_t aOpcode); //6XNN
	void addValueToReg(uint16_t aOpcode); //7XNN
	void setRegToRegValue(uint16_t aOpcode); //8XY0
	void setRegToRegValueOR(uint16_t aOpcode); //8XY1
	void setRegToRegValueAND(uint16_t aOpcode); //8XY2
	void setRegToRegValueXOR(uint16_t aOpcode); //8XY3
	void addRegToRegCarry(uint16_t aOpcode); //8XY4
	void subRegFromRegCarry(uint16_t aOpcode); //8XY5
	void shiftRegRightCarry(uint16_t aOpcode); //8XY6
	void setSubRegToRegCarry(uint16_t aOpcode); //8XY7
	void shiftRegLeftCarry(uint16_t aOpcode); //8XYE
	void skipNextInstructionNoMatchReg(uint16_t aOpcode); //9XY0
	void setMemoryPointer(uint16_t aOpcode); //ANNN
	void jumpToAddressAddReg(uint16_t aOpcode); //BNNN
	void setRegToRandValueAND(uint16_t aOpcode); //CXNN
	void displaySpriteFromMem(uint16_t aOpcode); //Dxyn
	void skipIfPressed(uint16_t aOpcode); //Ex9E
	void skipIfNotPressed(uint16_t aOpcode); //ExA1
	void timerValueInReg(uint16_t aOpcode); //Fx07
	void waitForKeyPress(uint16_t aOpcode); //
	void setDelayTimerToRegValue(uint16_t aOpcode); //Fx15s
	void setSoundTimerToRegValue(uint16_t aOpcode); //Fx18
	void addRegValueToMemPointer(uint16_t aOpcode); //Fx1E
	void setMemPointerToSprite(uint16_t aOpcode); //Fx29
	void storeBDCInRegs(uint16_t aOpcode); //Fx33
	void storeRegsInMemory(uint16_t aOpcode); //Fx55
	void putMemoryValuesInRegs(uint16_t aOpcode); //Fx65

	const static uint8_t* fontset;

};



#endif /* CHIP8_HPP_ */
